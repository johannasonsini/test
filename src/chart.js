import Chart from "chart.js/auto";

const ctx = document.getElementById("myChart").getContext("2d");

function initChart(context) {
    return new Chart(context, {
        type: "line",
        data: {
            labels,
            datasets: [{
                label: "",
                data,

                borderColor: "rgb(210, 46, 46)",
                tension: 0.1,
            }, ],
        },
        options: {
            scales: {
                x: {
                    beginAtZero: true,
                    max: 100,
                },
                y: {
                    beginAtZero: true,
                    max: 100,
                },
            },
        },
    });
}

const data = [];
const labels = [];

const chart = initChart(ctx);
const sck = new WebSocket("ws://localhost:9000");

sck.addEventListener("message", ev => {
    const d = JSON.parse(ev.data);

    data.push(d.value);
    labels.push(d.time);
    chart.update();
});